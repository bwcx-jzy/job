package cn.jiangzeyin.entity;

import cn.jiangzeyin.job.JobUtil;
import cn.jiangzeyin.log.JobLog;
import cn.jiangzeyin.system.JobPropertiesInfo;
import cn.jiangzeyin.system.SystemJobManager;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.quartz.Trigger.TriggerState;

import java.text.SimpleDateFormat;

/**
 * 调度运行结果状态信息
 *
 * @author jiangzeyin
 */
public class JobRunResultStatus {
    /**
     * 名称描述
     */
    private String name;
    /**
     * 结束时间
     */
    private Long endTime;
    /**
     * 运行次数
     */
    private long runCount;
    /**
     * 调度context
     */
    private JobExecutionContext context;
    /**
     * key
     */
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getRunCount() {
        return runCount;
    }

    public void setRunCount(long runCount) {
        this.runCount = runCount;
    }

    /**
     * 获取调度内部状态信息
     *
     * @return state
     * @author jiangzeyin
     */
    public String getTriggerState() {
        String str = "获取失败";
        if (context == null) {
            return "没有任务状态";
        }
        TriggerState triggerState;
        try {
            triggerState = JobUtil.getScheduler().getTriggerState(context.getTrigger().getKey());
        } catch (SchedulerException e) {
            JobLog.getInstance().error("获取状态失败", e);
            return "获取状态失败";
        }
        if (triggerState == null) {
            return str;
        }
        if (triggerState == TriggerState.NONE) {
            str = "没有";
        } else if (triggerState == TriggerState.NORMAL) {
            str = "正常";
        } else if (triggerState == TriggerState.PAUSED) {
            str = "暂停";
        } else if (triggerState == TriggerState.COMPLETE) {
            str = "完成";
        } else if (triggerState == TriggerState.ERROR) {
            str = "出错";
        } else if (triggerState == TriggerState.BLOCKED) {
            str = "阻塞";
        }
        return str;
    }

    /**
     * 获取调度内部运行时间
     *
     * @return long
     * @author jiangzeyin
     */
    public long getRunTime() {
        if (context == null) {
            return -1;
        }
        return context.getJobRunTime();
    }

    public void setContext(JobExecutionContext context) {
        this.context = context;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 开始时间
     *
     * @return string
     * @author jiangzeyin
     */
    public String getStartTime() {
        if (context == null) {
            return "没有运行";
        }
        // startTime;
        return formatTime(context.getTrigger().getPreviousFireTime().getTime());
    }

    /**
     * 调度预计下次运行时间
     *
     * @return string
     * @author jiangzeyin
     */
    public String getNextRunTime() {
        if (context == null) {
            return "";
        }
        // NextRunTime;
        return formatTime(context.getTrigger().getNextFireTime().getTime());
    }

    /**
     * 调度运行结束时间
     *
     * @return str
     * @author jiangzeyin
     */
    public String getEndTime() {
        if (endTime == null) {
            return "运行还没有结束";
        }
        // endTime;
        return formatTime(endTime);
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public JobExecutionContext getContext() {
        return context;
    }

    /**
     * @param date time
     * @return string
     * @author jiangzeyin
     */
    private static String formatTime(long date) {
        String pattern = SystemJobManager.getValue(JobPropertiesInfo.JOB_RUN_STATUS_ENTITY_TIME_FORM_MART, JobPropertiesInfo.JOB_RUN_STATUS_ENTITY_TIME_FORM_MART_DEFAULT_VALUE);
        if (pattern == null) {
            return date + "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }
}
