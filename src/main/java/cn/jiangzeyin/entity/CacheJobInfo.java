package cn.jiangzeyin.entity;

import cn.jiangzeyin.job.JobUtil;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;

/**
 * 调度运行信息
 *
 * @author jiangzeyin
 */
public class CacheJobInfo {
    /**
     * 描述名称
     */
    private String name;
    /**
     * 数据id
     */
    private int id;
    /**
     * 监听控制使用其key
     */
    private CronTrigger trigger;
    /**
     * 监听控制使用其key
     */
    private JobDetail job;
    /**
     * @see JobUtil#getJobKeyName(cn.jiangzeyin.entity.IQuartzInfo)
     */
    private String key;
    /**
     * 运行的class
     */
    private String runClass;
    /**
     * 时间表达式
     */
    private String cron;
    /**
     * @see cn.jiangzeyin.entity.IQuartzInfo.Status
     */
    private int status;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public JobDetail getJob() {
        return job;
    }

    public void setJob(JobDetail job) {
        this.job = job;
    }

    public CronTrigger getTrigger() {
        return trigger;
    }

    public void setTrigger(CronTrigger trigger) {
        this.trigger = trigger;
    }

    public String getRunClass() {
        return runClass;
    }

    public void setRunClass(String runClass) {
        this.runClass = runClass;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }
}