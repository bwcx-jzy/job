package cn.jiangzeyin.system;

import cn.jiangzeyin.entity.IQuartzInfo;
import com.alibaba.fastjson.JSONArray;

import java.util.List;

/**
 * 调度运行中
 * Created by jiangzeyin on 2017/8/22.
 *
 * @author jiangzeyin
 */
public class JobDataUtil implements JobDataInterface {
    /**
     * 接口
     */
    private volatile static JobDataInterface jobDataInterface;
    /**
     * 使用对象
     */
    private volatile static JobDataUtil DATA_UTIL;

    public static JobDataInterface getInstance() {
        if (DATA_UTIL == null) {
            synchronized (JobDataUtil.class) {
                if (DATA_UTIL == null) {
                    DATA_UTIL = new JobDataUtil();
                }
            }
        }
        return DATA_UTIL;
    }

    private JobDataUtil() {
    }

    public static void setDataInterface(JobDataInterface dataInterface) {
        JobDataUtil.jobDataInterface = dataInterface;
    }

    @Override
    public List<IQuartzInfo> getAll(String debug) {
        if (jobDataInterface == null) {
            throw new RuntimeException("please set DataInterface");
        }
        return jobDataInterface.getAll(debug);
    }

    @Override
    public void update(Class<?> entityClass, int jobId, String data) {
        if (jobDataInterface == null) {
            throw new RuntimeException("please set DataInterface");
        }
        jobDataInterface.update(entityClass, jobId, data);
    }

    @Override
    public JSONArray getJobRunData(Class<?> entityClass, int jobId) {
        if (jobDataInterface == null) {
            throw new RuntimeException("please set DataInterface");
        }
        return jobDataInterface.getJobRunData(entityClass, jobId);
    }

    @Override
    public Class getJobDefaultClass() {
        if (jobDataInterface == null) {
            throw new RuntimeException("please set DataInterface");
        }
        return jobDataInterface.getJobDefaultClass();
    }
}
