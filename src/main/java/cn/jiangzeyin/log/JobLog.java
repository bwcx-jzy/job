package cn.jiangzeyin.log;

/**
 * Created by jiangzeyin on 2017/8/14.
 *
 * @author jiangzeyin
 */
public class JobLog implements JobLogInterface {

    private JobLog() {
    }

    private static final JobLog SYSTEM_DB_LOG = new JobLog();
    private static JobLogInterface jobLogInterface;

    public static void setDbLogInterface(JobLogInterface dbLogInterface) {
        JobLog.jobLogInterface = dbLogInterface;
    }

    public static JobLog getInstance() {
        return SYSTEM_DB_LOG;
    }

    @Override
    public void info(Object object) {
        if (jobLogInterface == null) {
            System.err.println("please set dbLogInterface");
            System.err.println(object);
            return;
        }
        jobLogInterface.info(object);
    }

    @Override
    public void error(String msg, Throwable t) {
        if (jobLogInterface == null) {
            System.err.println("please set dbLogInterface");
            System.err.println(msg);
            if (t != null) {
                t.printStackTrace();
            }
            return;
        }
        jobLogInterface.error(msg, t);
    }

    @Override
    public void warn(Object msg) {
        if (jobLogInterface == null) {
            System.err.println("please set dbLogInterface");
            System.err.println(msg);
            return;
        }
        jobLogInterface.warn(msg);
    }

    @Override
    public void warn(String msg, Throwable t) {
        if (jobLogInterface == null) {
            System.err.println("please set dbLogInterface ");
            System.err.println(msg);
            if (t != null) {
                t.printStackTrace();
            }
            return;
        }
        jobLogInterface.warn(msg, t);
    }
}


