package cn.jiangzeyin.log;

/**
 * Created by jiangzeyin on 2017/8/14.
 *
 * @author jiangzeyin
 */
public interface JobLogInterface {
    /**
     * info
     *
     * @param object obj
     */
    void info(Object object);

    /**
     * error
     *
     * @param msg msg
     * @param t   t
     */
    void error(String msg, Throwable t);

    /**
     * warn
     *
     * @param msg msg
     */
    void warn(Object msg);

    /**
     * warn
     *
     * @param msg msg
     * @param t   t
     */
    void warn(String msg, Throwable t);
}