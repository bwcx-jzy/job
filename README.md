# job
一个动态管理quartz job 工具
以接口的形式动态监听每个调度状态，动态开启，暂停，恢复

cn.jiangzeyin.system.SystemJobManager  初始化调度工具

cn.jiangzeyin.system.JobDataUtil  调度数据查询动态获取（接口）

cn.jiangzeyin.job.BaseJob  公共job 接口类

cn.jiangzeyin.job.SystemJobListening  调度运行状态监听

cn.jiangzeyin.job.JobUtil  处理调度信息工具类

cn.jiangzeyin.entity.IQuartzInfo 调度信息接口类

cn.jiangzeyin.log.JobLog  调度工具日志接口